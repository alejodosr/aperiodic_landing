	# - Try to find LIBSVM library
# Once done this will define
#  LIBSVM_FOUND - System has the library
#  LIBSVM_INCLUDE_DIR - The include directories
#  LIBSVM_LIBRARY - The library(ies)

MESSAGE(STATUS "Aruco_DIR is : " ${Aruco_DIR})

#find_package(PkgConfig)
#pkg_check_modules(PC_LIBSVM QUIET libsvm-3.21 libsvm)
#set(LIBSVM_DEFINITIONS ${PC_LIBSVM_CFLAGS_OTHER})

find_path(Aruco_INCLUDE_DIR aruco.h cvdrawingutils.h
			 HINTS  ~/sources/aruco-2.0.17/src /usr/local/include /usr/include usr/
          PATH_SUFFIXES Aruco)
MESSAGE(STATUS "Aruco_INCLUDE_DIR is : " ${Aruco_INCLUDE_DIR})
			
# find library file
#FILE(GLOB_RECURSE LIBSVM_LIBRARY ${LIBSVM_INCLUDE_DIR}/build/*.a)


find_library(Aruco_LIBRARY
	NAMES
		libaruco.so
	PATHS
		${Aruco_INCLUDE_DIR}/build/
		/usr/bin
		/usr/lib
		/usr/local/lib
		~/sources/aruco-2.0.17/build/src
)

MESSAGE(STATUS "Aruco_LIBRARY is : " ${Aruco_LIBRARY})

			
if(Aruco_LIBRARY AND Aruco_INCLUDE_DIR)
	mark_as_advanced(Aruco_LIBRARY Aruco_INCLUDE_DIR)
	set(Aruco_FOUND 1)
else()
	set(Aruco_FOUND 0)
endif()

# ----------------------------------------------------------------------
# Display status
# ----------------------------------------------------------------------
if(NOT Aruco_FOUND)
	if(Aruco_FIND_REQUIRED)
		message(FATAL_ERROR "Aruco was not found. Please build dependencies first, or specify the path manually.")
	endif()
endif()

