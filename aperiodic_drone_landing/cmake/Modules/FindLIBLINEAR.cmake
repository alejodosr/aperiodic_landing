# - Try to find LIBLINEAR library
# Once done this will define
#  LIBLINEAR_FOUND - System has the library
#  LIBLINEAR_INCLUDE_DIR - The include directories
#  LIBLINEAR_LIBRARY - The library(ies)

MESSAGE(STATUS "LIBLINEAR_DIR is : " ${LIBLINEAR_DIR})

#find_package(PkgConfig)
#pkg_check_modules(PC_LIBLINEAR QUIET liblinear-2.1 liblinear)
#set(LIBLINEAR_DEFINITIONS ${PC_LIBLINEAR_CFLAGS_OTHER})

find_path(LIBLINEAR_INCLUDE_DIR linear.h
			 HINTS  ~/sources/liblinear-2.1 /usr/local/include /usr/include usr/
          PATH_SUFFIXES LIBLINEAR)
MESSAGE(STATUS "LIBLINEAR_INCLUDE_DIR is : " ${LIBLINEAR_INCLUDE_DIR})
			
# find library file
#FILE(GLOB_RECURSE LIBLINEAR_LIBRARY ${LIBLINEAR_INCLUDE_DIR}/build/*.a)


find_library(LIBLINEAR_LIBRARY
	NAMES
		liblinear.so.3
	PATHS
		${LIBLINEAR_INCLUDE_DIR}/build/
		/usr/bin
		/usr/lib
		/usr/local/lib
		~/sources/liblinear-2.1
)

MESSAGE(STATUS "LIBLINEAR_LIBRARY is : " ${LIBLINEAR_LIBRARY})

			
if(LIBLINEAR_LIBRARY AND LIBLINEAR_INCLUDE_DIR)
	mark_as_advanced(LIBLINEAR_LIBRARY LIBLINEAR_INCLUDE_DIR)
	set(LIBLINEAR_FOUND 1)
else()
	set(LIBLINEAR_FOUND 0)
endif()

# ----------------------------------------------------------------------
# Display status
# ----------------------------------------------------------------------
if(NOT LIBLINEAR_FOUND)
	if(LIBLINEAR_FIND_REQUIRED)
		message(FATAL_ERROR "LIBLINEAR was not found. Please build dependencies first, or specify the path manually.")
	endif()
endif()

