# - Try to find LIBSVM library
# Once done this will define
#  LIBSVM_FOUND - System has the library
#  LIBSVM_INCLUDE_DIR - The include directories
#  LIBSVM_LIBRARY - The library(ies)

MESSAGE(STATUS "LIBSVM_DIR is : " ${LIBSVM_DIR})

#find_package(PkgConfig)
#pkg_check_modules(PC_LIBSVM QUIET libsvm-3.21 libsvm)
#set(LIBSVM_DEFINITIONS ${PC_LIBSVM_CFLAGS_OTHER})

find_path(LIBSVM_INCLUDE_DIR svm.h
			 HINTS  ~/sources/libsvm-3.21 /usr/local/include /usr/include usr/
          PATH_SUFFIXES LIBSVM)
MESSAGE(STATUS "LIBSVM_INCLUDE_DIR is : " ${LIBSVM_INCLUDE_DIR})
			
# find library file
#FILE(GLOB_RECURSE LIBSVM_LIBRARY ${LIBSVM_INCLUDE_DIR}/build/*.a)


find_library(LIBSVM_LIBRARY
	NAMES
		libsvm.so.2
	PATHS
		${LIBSVM_INCLUDE_DIR}/build/
		/usr/bin
		/usr/lib
		/usr/local/lib
		~/sources/libsvm-3.21
)

MESSAGE(STATUS "LIBSVM_LIBRARY is : " ${LIBSVM_LIBRARY})

			
if(LIBSVM_LIBRARY AND LIBSVM_INCLUDE_DIR)
	mark_as_advanced(LIBSVM_LIBRARY LIBSVM_INCLUDE_DIR)
	set(LIBSVM_FOUND 1)
else()
	set(LIBSVM_FOUND 0)
endif()

# ----------------------------------------------------------------------
# Display status
# ----------------------------------------------------------------------
if(NOT LIBSVM_FOUND)
	if(LIBSVM_FIND_REQUIRED)
		message(FATAL_ERROR "LIBSVM was not found. Please build dependencies first, or specify the path manually.")
	endif()
endif()

