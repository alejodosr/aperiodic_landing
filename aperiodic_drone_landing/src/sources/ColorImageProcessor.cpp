#include "ColorImageProcessor.h"


ColorImageProcessor::ColorImageProcessor(void)
{
}


cv::Mat ColorImageProcessor::ProcessInOpponentColorSpace(cv::Mat &I)
{
	std::vector<cv::Mat> canales_rgb;
	std::vector<cv::Mat> canales_op;
	cv::Mat I_O1_thresholded, I_O2_thresholded, I_O3_thresholded;

	cv::split(I, canales_rgb);
	cv::Mat I_B = canales_rgb[0];
	cv::Mat I_G = canales_rgb[1];
	cv::Mat I_R = canales_rgb[2];
	cv::Mat I_OP_result;

	cv::Mat I_O1 = (I_R - I_G)/(double)cv::sqrt(2.0);
	cv::Mat I_O2 = (I_R + I_G - 2*I_B)/(double)cv::sqrt(6.0);
	cv::Mat I_O3 = (I_R + I_G + I_B)/(double)cv::sqrt(3.0);
	cv::Mat I_O4 = (I_B - I_G)/(double)cv::sqrt(2.0);
	canales_op.push_back(I_O1);
	canales_op.push_back(I_O2);
	canales_op.push_back(I_O3);
    //imshow("I_O1", I_O1);
    //imshow("I_O2", I_O2);
    //imshow("I_O3", I_O3);
    //imshow("I_O4", I_O4);

	cv::threshold(I_O1, I_O1_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_O2, I_O2_thresholded, 50, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_O3, I_O3_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	//cv::imshow("I channel O1 (Lab) Thresholded", I_O1_thresholded);
	//cv::imshow("I channel O2 (Lab) Thresholded", I_O2_thresholded);
    //cv::imshow("I channel O3 (Lab) Thresholded", I_O3_thresholded);


	cv::Mat I_op;
	cv::merge(canales_op, I_op);
	return I_OP_result;
}


cv::Mat ColorImageProcessor::ProcessRedInOpponentColorSpace(cv::Mat &I)
{
	std::vector<cv::Mat> canales_rgb;
	std::vector<cv::Mat> canales_op;

	cv::split(I, canales_rgb);
	cv::Mat I_B = canales_rgb[0];
	cv::Mat I_G = canales_rgb[1];
	cv::Mat I_R = canales_rgb[2];
	cv::Mat I_OP_result;

	cv::Mat O1 = (I_R - I_G)/(double)cv::sqrt(2.0);
	imshow("O1 OP", O1);


	cv::Mat I_O1_thresholded;
    cv::threshold(O1, I_O1_thresholded, 20, 255, cv::THRESH_BINARY);
	cv::morphologyEx(I_O1_thresholded, I_O1_thresholded, cv::MORPH_CLOSE, cv::Mat(3,3, CV_8U, cv::Scalar(1)));


	cv::imshow("I channel O1 (Opponent) Thresholded", I_O1_thresholded);


	return I_O1_thresholded;
}

cv::Mat ColorImageProcessor::ProcessBlueInOpponentColorSpace(cv::Mat &I)
{
	std::vector<cv::Mat> canales_rgb;
	std::vector<cv::Mat> canales_op;

	cv::split(I, canales_rgb);
	cv::Mat I_B = canales_rgb[0];
	cv::Mat I_G = canales_rgb[1];
	cv::Mat I_R = canales_rgb[2];
	cv::Mat I_OP_result;

	cv::Mat O4 = (I_B - I_G)/(double)cv::sqrt(2.0);
    //imshow("O4 OP", O4);

	cv::Mat I_O4_thresholded;
    cv::threshold(O4, I_O4_thresholded, 10, 255, cv::THRESH_BINARY);
	cv::morphologyEx(I_O4_thresholded, I_O4_thresholded, cv::MORPH_CLOSE, cv::Mat(3,3, CV_8U, cv::Scalar(1)));


    //cv::imshow("I channel O4 (Opponent) Thresholded", I_O4_thresholded);

	return I_O4_thresholded;
}

cv::Mat ColorImageProcessor::ProcessInRGBColorSpace(cv::Mat &I)
{
	cv::Mat I_hsv;
	cv::Mat I_R_thresholded, I_G_thresholded, I_B_thresholded;
	cv::Mat I_S_thresholded;
	cv::Mat I_and, I_xor_RandB;
	cv::Mat I_G_thresholded_dilated;
	cv::Mat I_color_result;

	cv::cvtColor(I, I_hsv, CV_BGR2HSV);
	std::vector<cv::Mat> canales_rgb;
	std::vector<cv::Mat> canales_hsv;
	cv::split(I, canales_rgb);
	cv::split(I_hsv, canales_hsv);
	cv::Mat I_B = canales_rgb[0];
	cv::Mat I_G = canales_rgb[1];
	cv::Mat I_R = canales_rgb[2];
	cv::Mat I_H = canales_hsv[0];
	cv::Mat I_S = canales_hsv[1];
	cv::Mat I_V = canales_hsv[2];
	//cv::equalizeHist(I_V,I_V_eq);
	//imshow("I_R",I_R);
	//imshow("I_G",I_G);
	//imshow("I_B",I_B);
	//imshow("I_H",I_H);
	//imshow("I_S",I_S);
	//imshow("I_V",I_V);


	cv::threshold(I_R, I_R_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_G, I_G_thresholded, 50, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_B, I_B_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_S, I_S_thresholded, 200, 255, cv::THRESH_BINARY);
	cv::bitwise_xor(I_R_thresholded, I_B_thresholded, I_xor_RandB);
	cv::bitwise_and(I_S_thresholded, I_xor_RandB, I_color_result);

	cv::Mat element9(3,3,CV_8U,cv::Scalar(1));
	cv::dilate(I_G_thresholded, I_G_thresholded_dilated, element9);
	cv::morphologyEx(I_color_result, I_color_result, cv::MORPH_OPEN, cv::Mat(2,2, CV_8U, cv::Scalar(1)));
	//cv::dilate(I_xor_RandB, I_xor_RandB, element9);

	//cv::imshow("I channel R (RGB) Thresholded", I_R_thresholded);
	//cv::imshow("I channel G (RGB) Thresholded", I_G_thresholded);
	//cv::imshow("I channel B (RGB) Thresholded", I_B_thresholded);
	//cv::imshow("I channel S (HSV) Thresholded", I_S_thresholded);
	//cv::imshow("I AND", I_and);
	//cv::imshow("I XOR channel R and B", I_xor_RandB);
	cv::imshow("I COLOR RESULT", I_color_result);

	return I_color_result;
}

cv::Mat ColorImageProcessor::ProcessInLabColorSpace(cv::Mat &I)
{
	cv::Mat I_Lab;
	cv::Mat I_L_thresholded, I_a_thresholded, I_b_thresholded;
	cv::Mat I_color_result;

    cv::cvtColor(I, I_Lab, CV_BGR2Lab);
	std::vector<cv::Mat> canales_Lab;
	cv::split(I_Lab, canales_Lab);
	cv::Mat I_L = canales_Lab[0];
	cv::Mat I_a = canales_Lab[1];
	cv::Mat I_b = canales_Lab[2];
	//cv::equalizeHist(I_V,I_V_eq);
    //imshow("I_L (Lab)",I_L);
    //imshow("I_a (Lab)",I_a);
    //imshow("I_b (Lab)",I_b);

	cv::threshold(I_L, I_L_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_a, I_a_thresholded, 50, 255, cv::THRESH_BINARY_INV);
    cv::threshold(I_b, I_b_thresholded, 100, 255, cv::THRESH_BINARY_INV);
	//cv::imshow("I channel L (Lab) Thresholded", I_L_thresholded);
	//cv::imshow("I channel a (Lab) Thresholded", I_a_thresholded);
    //cv::imshow("I channel b (Lab) Thresholded", I_b_thresholded);

	return I_color_result;
}

cv::Mat ColorImageProcessor::ProcessInHLSColorSpace(cv::Mat &I)
{
	cv::Mat I_hls;
	cv::Mat I_H_thresholded, I_L_thresholded, I_S_thresholded;
	cv::Mat I_color_result;

	cv::cvtColor(I, I_hls, CV_BGR2HSV);
	std::vector<cv::Mat> canales_hls;
	cv::split(I_hls, canales_hls);
	cv::Mat I_H = canales_hls[0];
	cv::Mat I_L = canales_hls[1];
	cv::Mat I_S = canales_hls[2];
	//cv::equalizeHist(I_V,I_V_eq);
    //imshow("I_H (HLS)",I_H);
    //imshow("I_L (HLS)",I_L);
    //imshow("I_S (HLS)",I_S);

	cv::threshold(I_H, I_H_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_L, I_L_thresholded, 50, 255, cv::THRESH_BINARY_INV);
	cv::threshold(I_S, I_S_thresholded, 20, 255, cv::THRESH_BINARY_INV);
	//cv::imshow("I channel H (HLS) Thresholded", I_H_thresholded);
	//cv::imshow("I channel L (HLS) Thresholded", I_L_thresholded);
    //cv::imshow("I channel S (HLS) Thresholded", I_S_thresholded);

	return I_color_result;
}





ColorImageProcessor::~ColorImageProcessor(void)
{
}
