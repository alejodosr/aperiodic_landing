/*
 *
 *
 *  Created on: July 12th 2016
 *      Author: Milosevic Zorana
 */


#include "droneAperiodicLandingROSModule.h"



using namespace std;


//void DroneAperiodicLanding::droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
//{
//    current_drone_position_reference = (*msg);
//}


DroneAperiodicLanding::DroneAperiodicLanding() : DroneModule(droneModule::active)
{
    if(!init())
        std::cout<<"Error init"<<std::endl;

    return;
}


DroneAperiodicLanding::~DroneAperiodicLanding()
{
    close();
    return;
}

void DroneAperiodicLanding::readParameters()
{
    return;
}


bool DroneAperiodicLanding::init()
{
    measuredAltitude = 0.0;
    counted_platforms = 0;
    platform_size = 0;
    start_landing = false;
    enable_start = true;
    enable_stop = false;
    return true;
}


void DroneAperiodicLanding::open(ros::NodeHandle & nIn, std::string moduleName)
{
    DroneModule::open(nIn,moduleName);

    readParameters();

#ifdef SIM_MODE
    droneLidar          = n.subscribe("/px4flow/raw/px4Flow_pose_z", 1, &DroneAperiodicLanding::droneLidarCallback, this);
#else
    droneLidar          = n.subscribe("lightware_altitude", 1, &DroneAperiodicLanding::droneLidarCallback, this);
#endif
    //    droneRebroadCast    = n.subscribe("trajectoryControllerPositionReferencesRebroadcast", 1, &DroneAperiodicLanding::droneCurrentPositionRefsSubCallback,this);

    dronePoseSub        = n.subscribe("EstimatedPose_droneGMR_wrt_GFF",1, &DroneAperiodicLanding::dronePoseCallback, this);
    dronePosePub        = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1);
    DroneCommandPubl    = n.advertise<droneMsgsROS::droneCommand>("command/high_level",1, true);

    init();

    droneModuleOpened=true;

    thread_started = false;


    //Auto-Start module
    //moduleStarted=true;

    return;
}

void DroneAperiodicLanding::dronePoseCallback(const droneMsgsROS::dronePose &msg){
    current_pose = msg;
}


void DroneAperiodicLanding::DroneLanding()
{
    while(ros::ok()){
        if(start_landing && moduleStarted){
            //            bool landed = false;
            //            while(!landed){
            // Variable declaration
            droneMsgsROS::droneCommand DroneCommandMsgs;
            ros::Time now = ros::Time::now();
            if(((now - start_peak).toSec() > landing_time) &&
                    ((now - start_peak).toSec() < landing_time + ((double) PERCENTAGE_TOL * platform_size))){
                if (measuredAltitude < (float) SAFE_ALTITUDE){ // Drone has to land from a safe altitude
                    DroneCommandMsgs.command = droneMsgsROS::droneCommand::LAND;
                    //                DroneCommandMsgs.command = droneMsgsROS::droneCommand::EMERGENCY_STOP;
                    DroneCommandPubl.publish(DroneCommandMsgs);

                    // Testing
                    //droneMsgsROS::dronePositionRefCommandStamped ref;
                    //ref.header.stamp = ros::Time::now();
                    //ref.position_command.x = current_pose.x;
                    //ref.position_command.y = current_pose.y;
                    //ref.position_command.z = 0.3;
                    //dronePosePub.publish(ref);
                    //

                    start_landing = false;

#ifdef DEBUG_MODE
                    cout << "Platform size: " << platform_size << "\t Landing at: " << (now - start_peak).toSec() << endl;
#endif
                }
                else{   // Retry the landing
#ifdef DEBUG_MODE
                    cout << "Not at a safe altitude to land, retrying.." << endl;
#endif

#ifdef ALTITUDE_INTERFIERENCE
                    // Decrease the setpoint reference
                    droneMsgsROS::dronePositionRefCommandStamped ref;
                    ref.header.stamp = ros::Time::now();
                    ref.position_command.x = landing_x;
                    ref.position_command.y = landing_y;
                    ref.position_command.z = current_pose.z - (double) ALTITUDE_STEP;
                    dronePosePub.publish(ref);
#ifdef DEBUG_MODE
                    cout << "Altitude set point: " <<  ref.position_command.z << " m" << endl;
                    cout << "Platform size: " <<  platform_size << " s" << endl;
#endif
#endif
                    // Enable both
                    enable_start = true;
                    enable_stop = false;

                    // Disable landing
                    start_landing = false;
                }
            }
        }
    }

    //    // if the platform is under the drone and the height from the platform is less than some treshold,
    //    // we are ready for landing -> turn off the motors
    //    if (start_landing && measuredAltitude < LANDING_THRESHOLD)
    //    {

    //        //TODO: Publish the msg to GMP to land
    //        cout<<" --- READY FOR LANDING --- "<<endl;
    //        cout<<" --- TURNING OFF THE MOTORS --- "<<endl;
    //        DroneCommandMsgs.command = droneMsgsROS::droneCommand::LAND;
    //        DroneCommandPubl.publish(DroneCommandMsgs);


    //    }

}

#ifdef SIM_MODE
void DroneAperiodicLanding::droneLidarCallback(const geometry_msgs::PoseStamped &msg)
{
    /// If module started
    if (moduleStarted && MyHelipadDetectorROSModule.getEstimatedSpeedFlag()){

        /// Store the last measured altitude
        measuredAltitude = msg.pose.position.z;
#else
void DroneAperiodicLanding::droneLidarCallback(const sensor_msgs::Range &msg)
{
    /// If module started
    if (moduleStarted){

        /// Store the last measured altitude
        measuredAltitude = msg.range;
#endif

#ifdef ALTITUDE_INTERFIERENCE
                    // Decrease the setpoint reference
                    droneMsgsROS::dronePositionRefCommandStamped ref;
                    ref.header.stamp = ros::Time::now();
                    ref.position_command.x = landing_x;
                    ref.position_command.y = landing_y;
                    ref.position_command.z = (double) ALTITUDE_TO_LAND;
                    dronePosePub.publish(ref);
#endif

        /// Store the values in a buffer
        if(altitudes.size() < BUFFER_SIZE){
            altitudes.push_back(measuredAltitude);
        }
        else{
            // Shift the vector
            for(int i = 0; i < altitudes.size() - 1; i++){
                altitudes[i] = altitudes[i + 1];
            }
            altitudes[altitudes.size() - 1] = measuredAltitude;
        }


        /// If the buffer is full, start the calculations
        if(altitudes.size() == BUFFER_SIZE){

            // Compute the diffference in altitude
            double diff_altitude;
            diff_altitude = altitudes[altitudes.size() - 1] - altitudes[0];

            // if the difference between current height and previous one is negative and
            // that distance is bigger that some treshold, that means the drone is on top of the platform
            if (diff_altitude < 0 && abs(diff_altitude) >= (double) ALTITUDE_THRESHOLD && enable_start){
                // Store the time
                start_peak = ros::Time::now();

                // Enable stop
                enable_start = false;
                enable_stop = true;

                // Start landing


                // Compute the landing time
                double v_landing = MyHelipadDetectorROSModule.getEstimatedSpeed();
                landing_time = (( (double) SAFETY_DISTANCE - (double) LANDING_GEAR_WIDTH / 2.d) / v_landing) - (double) TIME_TO_FALL;

                // Debug
                cout << "v_landing: " << v_landing << endl;
                cout << "landing size (s): " << platform_size << endl;
                cout << "Time to land (s): " << landing_time << endl;

                if(landing_time > 0){

                    // Disable both
                    enable_start = false;
                    enable_stop = false;

                    // Enable landing
                    start_landing = true;

                    // Launch thread
                    if(!thread_started){
                        landing_x = current_pose.x;
                        landing_y = current_pose.y;
                        landing_thread = boost::thread(&DroneAperiodicLanding::DroneLanding, this);
                        thread_started = true;
                    }

#ifdef DEBUG_MODE
                    cout << "Landing started" << endl;
#endif

                }
                else{
#ifdef DEBUG_MODE
                    cout << "ALERT: not possible to land, moving platform too fast!" << endl;
#endif
                    // Reset counted platforms
                    counted_platforms = 0;
                }


            }

            // if the difference between current height and previous one is positive and
            // it's bigger that some treshold, that means the drone is no longer on top of the platform
            else if (diff_altitude > 0 && abs(diff_altitude) > (double) ALTITUDE_THRESHOLD && enable_stop){
                // Store the time
                stop_peak = ros::Time::now();

                // Enable start
                enable_start = true;
                enable_stop = false;

                // Increase the counted platforms
                counted_platforms++;

#ifdef DEBUG_MODE
                cout << "Counted platforms: " << counted_platforms << endl;
#endif

                // Store the size of the platform
                platform_sizes.push_back((stop_peak - start_peak).toSec());

                // Prevent from landing
                start_landing = false;
            }
        }
    }
}

void DroneAperiodicLanding::close()
{

    DroneModule::close();
    return;
}


bool DroneAperiodicLanding::resetValues()
{

    return true;

}


bool DroneAperiodicLanding::run()
{

    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}
