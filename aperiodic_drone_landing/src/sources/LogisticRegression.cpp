#include "LogisticRegression.h"
using namespace std;

LogisticRegression::LogisticRegression(void)
{
}

void LogisticRegression::LoadModel()
{
    Model_name = "resources/Modelo_LR_Helipad_OnlyFonts_5";
	const char * Model_name_c = Model_name.c_str();
	model = load_model(Model_name_c);
	if(model != NULL)
        cout<<"Modelo Logistic Regression creado correctamente! POLYMORPHISM"<<endl;
    else
        cout<<"Model NOT found"<<endl;

}

void LogisticRegression::SetModel(std::string Model_name)
{
	const char * Model_name_c = Model_name.c_str();
    cout<<"Model Name: "<<Model_name.c_str()<<endl;
	model = load_model(Model_name_c);
	if(model != NULL)
        cout<<"Modelo Logistic Regression creado correctamente! POLYMORPHISM"<<endl;
    else
        cout<<"Model NOT found"<<endl;
}

void LogisticRegression::predict(std::vector<float> descriptor)
{
	probs.clear();

    cout<<"Logistic Regression prediction..."<<endl;
    testnode = new feature_node[descriptor.size()+1];
	int col=0;
	for(col=0;col<descriptor.size();col++)
	{
		testnode[col].index=col+1;
		testnode[col].value=descriptor[col];
		//testnode[col].value=descriptor_Mat.at<float>(0,col);
	}
	testnode[col].index=-1;
	prob_estimates = new double[get_nr_class(model)];
	double retval = predict_probability(model,testnode,prob_estimates);

	for(int i=0;i<get_nr_class(model);i++)
		probs.push_back(prob_estimates[i]);

	delete[] testnode;
	delete[] prob_estimates;

}

LogisticRegression::~LogisticRegression(void)
{
}
