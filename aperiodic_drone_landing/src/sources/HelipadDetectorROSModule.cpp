#include "HelipadDetectorROSModule.h"
using namespace std;

HelipadDetectorROSModule::HelipadDetectorROSModule()
{

}


HelipadDetectorROSModule::~HelipadDetectorROSModule()
{

}



void HelipadDetectorROSModule::open(ros::NodeHandle & nIn)
{
    readParameters();
#ifdef GAZEBO_CAM
    helipad_recognized_image_pub = nIn.advertise<sensor_msgs::Image>("/image_processed/helipad_recognized_bottom_camera", 1, this);
    TwistPubl = nIn.advertise<geometry_msgs::PoseStamped>("/estimated_speed/", 1, this);
    drone_image_from_bottom_camera_subs = nIn.subscribe("/bottom_cam/image_raw", 1, &HelipadDetectorROSModule::droneImageFromBottomCameraCallback, this);
#else
    helipad_recognized_image_pub = nIn.advertise<sensor_msgs::Image>("/image_processed/helipad_recognized_bottom_camera", 1, this);
    drone_image_from_bottom_camera_subs = nIn.subscribe("/usb_cam/image_rect_color", 1, &HelipadDetectorROSModule::droneImageFromBottomCameraCallback, this);
#endif

    setEstimatedSpeedFlag(false);

}


void HelipadDetectorROSModule::readParameters()
{
    std::string Model_Name;
    ros::param::get("~model_name", Model_Name);
    cout<<"Model Name (readParameters): "<<Model_Name<<endl;
    objectDetector.setModelName("../../../../src/Helipad_Detection/resources/Modelo_LR_Helipad_48x48_7");
}

void HelipadDetectorROSModule::droneImageFromBottomCameraCallback(const sensor_msgs::Image &msg)
{
    if (!getEstimatedSpeedFlag()){
        cv_bridge::CvImagePtr cvObject;
        try
        {
            cvBottomImage = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            cvObject = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);

        }
        catch (cv_bridge::Exception& e)
        {

            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        bottom_camera_image = cvBottomImage->image;
        //cv::imshow("Bottom Camera Image", bottom_camera_image);
        //cv::waitKey(5);

        cv::Mat I;
        bottom_camera_image.copyTo(I);

#ifdef COMPLETE_TEST

        // Introduce points in space
        std::vector<cv::Point3f> points3D;
        points3D.push_back(cv::Point3f(0,0,0));
        points3D.push_back(cv::Point3f(HELIPAD_SIZE,0,0));
        points3D.push_back(cv::Point3f(HELIPAD_SIZE,HELIPAD_SIZE,0));
        points3D.push_back(cv::Point3f(0,HELIPAD_SIZE,0));

        cv::Mat I_helipad;
        I.copyTo(I_helipad);

        bool found = false;
        std::vector<cv::Point2f> points2D;
        cv::Point2f *p;

#ifdef WITH_ARUCO
        aruco::MarkerDetector MDetector;
        std::vector<aruco::Marker> Markers;

        MDetector.detect(I_helipad,Markers);



        //        for (unsigned int i=0;i<Markers.size();i++) {
        //            Markers[i].draw(I_helipad,Scalar(0,0,255),2);
        //        }

        if (Markers.size() > 0){
            aruco::Marker marker = Markers.back();
            p = marker.data();
            //            for(int i=0; i<corner.size(); i++){
            cv::circle(I_helipad, *p, 3, cv::Scalar(255, 0, 0), 2);
            cv::circle(I_helipad, *(p+1), 3, cv::Scalar(255, 255, 0), 2);
            cv::circle(I_helipad, *(p+2), 3, cv::Scalar(0, 255, 0), 2);
            cv::circle(I_helipad, *(p+3), 3, cv::Scalar(0, 0, 255), 2);
            found = true;

            points2D.push_back(*p);
            points2D.push_back(*(p+1));
            points2D.push_back(*(p+2));
            points2D.push_back(*(p+3));
            //            }
        }
#else
        // Detect helipad
        cv::Rect roi = objectDetector.DetectHelipad(I_helipad);
        if (roi.width > 0)  found = true;
        points2D.push_back(cv::Point2f(roi.x, roi.y));
        points2D.push_back(cv::Point2f(roi.x, roi.y + roi.height - 1));
        points2D.push_back(cv::Point2f(roi.x + roi.width - 1, roi.y));
        points2D.push_back(cv::Point2f(roi.x + roi.width - 1, roi.y + roi.height - 1));
#endif

        // Extract pose relative to the camera
        if (found){
            pnp_extractor.setCameraParameters(camera_matrix, distortion_matrix);
            pnp_extractor.computePnP(points3D, points2D, PNP_RANSAC);

            //            // Estimate motion
            cv::Mat last_measured(3,1, CV_64F);
            (pnp_extractor.getTvec()).copyTo(last_measured);
            motion_estimator.updateMeasuredPosition(last_measured);

            cout << pnp_extractor.getTvec() << endl;


            // Represent measured motion
#ifdef WITH_ARUCO
            cv::Point2f point = points2D.front();
            actualPoint = cv::Point(point.x, point.y);
            cout << "actualPoint: " << actualPoint << endl;
#else
            actualPoint = cv::Point(roi.x, roi.y);
#endif
            if (previousPoint.x != 0 &&  previousPoint.y != 0){
                cv::line(I_trajectory, previousPoint, actualPoint, cv::Scalar(0,255,0));
            }
            else{
                // Run motion estimator
                motion_estimator.run(100.d, MOTION_KALMAN_FILTER);
            }
            previousPoint = actualPoint;

        }

        // Backprojection on the image plane
        if (motion_estimator.isRunning()){
            cv::Mat estimatedTvec = motion_estimator.getEstimatedPosition();
            // Debug
            //            cv::Mat estimatedTvec = pnp_extractor.getTvec();

            cv::Mat actualTvec = pnp_extractor.getTvec();   // Set actual tvec for Z coordinate

            cout << "Actual tvec: " << actualTvec << endl;

            //            cout << "Difference between estimated and actual:" << estimatedTvec - actualTvec << endl;
            // Debug
            if (actualTvec.at<double>(0) > max_x_actual) max_x_actual = actualTvec.at<double>(0);
            if (actualTvec.at<double>(1) > max_y_actual) max_y_actual = actualTvec.at<double>(1);
            if (actualTvec.at<double>(2) > max_z_actual) max_z_actual = actualTvec.at<double>(2);

            if (actualTvec.at<double>(0) > max_x_est) max_x_est = estimatedTvec.at<double>(0);
            if (actualTvec.at<double>(1) > max_y_est) max_y_est = estimatedTvec.at<double>(1);
            if (actualTvec.at<double>(2) > max_z_est) max_z_est = estimatedTvec.at<double>(2);

            cout << "Actual max tvec: " << max_x_actual << "\t" << max_y_actual << "\t" << max_z_actual << endl;
            cout << "Estimated max tvec: " << max_x_est << "\t" << max_y_est << "\t" << max_z_est << endl;
            //


            //            cv::Mat estimatedPoint3D = pnp_extractor.getTvec();
            std::vector<cv::Point2d> point2D;
            std::vector<cv::Point3d> point3D;
            //            cout << "Estimated point: " << estimatedPoint3D << endl;
            //            cv::Point3d p(estimatedPoint3D);
            point3D.push_back(cv::Point3d(0,0,0));
            point2D = pnp_extractor.project3Dto2D(point3D, estimatedTvec);
            point3D.clear();
            cv::Point2d temp = point2D.back();
            point2D.clear();

            //            cout << "temp: " << temp << endl;

            actualEstimatedPoint = cv::Point(temp.x,temp.y);
            if (previousEstimatedPoint.x != 0 &&  previousEstimatedPoint.y != 0){
                // Represent estimated motion
                cv::line(I_trajectory, previousEstimatedPoint, actualEstimatedPoint, cv::Scalar(0,0,255));
            }
            previousEstimatedPoint = actualEstimatedPoint;

            // Plot speed
            cv::Mat speed = motion_estimator.getEstimatedSpeed();
            double vx = speed.at<double>(0);
            double vy = speed.at<double>(1);

            // Publish in ROS
            geometry_msgs::PoseStamped msgs;
            msgs.header.stamp = ros::Time::now();
            msgs.pose.position.x = -vx;
            msgs.pose.position.y = -vy;
            TwistPubl.publish(msgs);

            // Store estimated speed value
            setEstimatedSpeed(sqrt(pow(vx, 2) + pow(vy, 2)));

            // Check distance is within limits
            cv::Mat pose = motion_estimator.getEstimatedPosition();
            double distance = sqrt(pow(pose.at<double>(0), 2) + pow(pose.at<double>(1), 2) + pow(pose.at<double>(2), 2));

            if (distance <= MIN_DISTANCE_TO_LAND){
                motion_estimator.kill();
                setEstimatedSpeedFlag(true);
            }

            std::ostringstream ss;
            ss << "Vx: " << vx;

            cv::putText(I_helipad, ss.str(), cvPoint(30,30),
                        cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);

            std::ostringstream ss2;
            ss2 << "Vy: " << vy;
            cv::putText(I_helipad, ss2.str(), cvPoint(30,60),
                        cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);

            std::ostringstream ss3;
            ss3 << "Vabs: " << sqrt(pow(vx, 2) + pow(vy, 2));
            cv::putText(I_helipad, ss3.str(), cvPoint(30,90),
                        cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);
        }

        // Show image
#if IMSHOW ==  1
        cv::add(I_helipad, I_trajectory, I_helipad);
        imshow("Helipad", I_helipad);
        imshow("Motion", I_trajectory);
#endif

        cvObject->image = I_helipad;
        helipad_recognized_image_pub.publish(cvObject->toImageMsg());

        char tecla = cv::waitKey(10);
#else
        objectDetector.DetectHelipad(I_detection);

        cvObject->image = I_detection;
        helipad_recognized_image_pub.publish(cvObject->toImageMsg());
#endif
    }
}

int HelipadDetectorROSModule::setEstimatedSpeed(double v){
    mtx_.lock();
    estimated_speed = v;
    mtx_.unlock();

    return SUCCESS;
}

double HelipadDetectorROSModule::getEstimatedSpeed(){
    double v;
    mtx_.lock();
    v = estimated_speed;
    mtx_.unlock();

    return v;
}

int HelipadDetectorROSModule::setEstimatedSpeedFlag(bool v){
    mtx_.lock();
    is_v_estimated = v;
    mtx_.unlock();

    return SUCCESS;
}

bool HelipadDetectorROSModule::getEstimatedSpeedFlag(){
    double v;
    mtx_.lock();
    v = is_v_estimated;
    mtx_.unlock();

    return v;
}
