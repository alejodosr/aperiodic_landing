#include "SVM_Classifier.h"
using namespace std;

SVM_Classifier::SVM_Classifier(void)
{
}

void SVM_Classifier::LoadModel()
{
	Model_name = "Modelo_SVM_Linear_1_HVTowers_48x96";
	const char * Model_name_c = Model_name.c_str();
	model = svm_load_model(Model_name_c);
	if(model != NULL)
		cout<<"Modelo SVM creado correctamente! POLIMORFISM"<<endl;
}

void SVM_Classifier::SetModel(std::string Model_name)
{
	const char * Model_name_c = Model_name.c_str();
	model = svm_load_model(Model_name_c);
	if(model != NULL)
		cout<<"Modelo SVM creado correctamente! POLIMORFISM"<<endl;
}


void SVM_Classifier::predict(std::vector<float> descriptor)
{
	probs.clear();

    cout<<"SVM prediction..."<<endl;
	testnode = new svm_node[descriptor.size()+1];
	int col = 0;
	for(col=0;col<descriptor.size();col++)
	{
		testnode[col].index=col+1;
		testnode[col].value=descriptor[col];
		//testnode_SVM[col].value=descriptors_Mat.at<float>(0,col);
	}
	testnode[col].index=-1;
	//window_class = svm_predict(model_SVM,testnode_SVM);

			
	prob_estimates = new double[svm_get_nr_class(model)];
	double retval = svm_predict_probability(model,testnode,prob_estimates);

	for(int i=0;i<svm_get_nr_class(model);i++)
		probs.push_back(prob_estimates[i]);

	delete[] testnode;
	delete[] prob_estimates;
}


SVM_Classifier::~SVM_Classifier(void)
{
}
