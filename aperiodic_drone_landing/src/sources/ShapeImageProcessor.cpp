#include "ShapeImageProcessor.h"
using namespace std;
using namespace cv;

#define IMSHOW_MODE 0

ShapeImageProcessor::ShapeImageProcessor(void)
{
}


ShapeImageProcessor::~ShapeImageProcessor(void)
{
}

cv::Mat ShapeImageProcessor::detectRectangles(Mat &I)
{
    vector<vector<Point> > rectangles;

    findRectangles(I, rectangles);
    cv::Mat I_mask = drawRectanglesMask(I, rectangles);
    //cv::Mat I_mask = drawRectanglesContour(I, rectangles);

    return I_mask;
}

void ShapeImageProcessor::findRectangles(const cv::Mat& image, std::vector<std::vector<cv::Point> >& rectangles)
{
   rectangles.clear();

    Mat pyr, timg, gray0(image.size(), CV_8U), gray;

    // down-scale and upscale the image to filter out the noise
    pyrDown(image, pyr, cv::Size(image.cols/2, image.rows/2));
    pyrUp(pyr, timg, image.size());
    std::vector<std::vector<cv::Point> > contours;

    // find rectangles in every color plane of the image
    for( int c = 0; c < 3; c++ )
    {
        int ch[] = {c, 0};
        mixChannels(&timg, 1, &gray0, 1, ch, 1);

        //cv::Canny(gray0, gray, 20, 200);
        cv::Canny(gray0, gray, 20, 50);
        dilate(gray, gray, Mat(), Point(-1,-1));
        //imshow("Canny", gray);

        // find contours and store them all as a list
        findContours(gray, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
        //findContours(gray, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

        vector<Point> approx;
        // test each contour
        for( size_t i = 0; i < contours.size(); i++ )
        {
            // approximate contour with accuracy proportional
            // to the contour perimeter
            approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true)*0.02, true);

            // square contours should have 4 vertices after approximation
            // relatively large area (to filter out noisy contours)
            // and be convex.
            // Note: absolute value of an area is used because
            // area may be positive or negative - in accordance with the
            // contour orientation
            if( approx.size() == 4 && fabs(contourArea(Mat(approx))) > 1000 && isContourConvex(Mat(approx)) )
            {
                double maxCosine = 0;

                for( int j = 2; j < 5; j++ )
                {
                    // find the maximum cosine of the angle between joint edges
                    double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                    maxCosine = MAX(maxCosine, cosine);
                }

                // if cosines of all angles are small
                // (all angles are ~90 degree) then write quandrange
                // vertices to resultant sequence
                if( maxCosine < 0.3 )
                    rectangles.push_back(approx);
            }

        }
    }


}

Mat ShapeImageProcessor::drawRectanglesMask(cv::Mat& image, const std::vector<std::vector<cv::Point> >& rectangles)
{
    cv::Mat I_mask = cv::Mat::zeros(image.size(), CV_8U);
    for( size_t i = 0; i < rectangles.size(); i++ )
    {
        const Point* p = &rectangles[i][0];
        int n = (int)rectangles[i].size();
        cv::fillConvexPoly(I_mask, p, n, Scalar(255));
    }

    if(IMSHOW_MODE)
    {
        imshow("Rectangles Mask", I_mask);
    }
    return I_mask;
}

cv::Mat ShapeImageProcessor::drawRectanglesContour(Mat& image, const vector<vector<Point> >& rectangles)
{
    cv::Mat I_mask = cv::Mat::zeros(image.size(), CV_8U);
    for( size_t i = 0; i < rectangles.size(); i++ )
    {
        const Point* p = &rectangles[i][0];
        int n = (int)rectangles[i].size();
        cv::polylines(I_mask, &p, &n, 1, true, Scalar(255), 3);
    }

    if(IMSHOW_MODE)
    {
        imshow("Rectangles contours", I_mask);
    }
    return I_mask;
}

double ShapeImageProcessor::angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}


