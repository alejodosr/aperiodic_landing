#include "HOG.h"
using namespace std;

HOG::HOG(void)
{
    winSize = cv::Size(48,48);
	blockSize = cv::Size(16,16);
	blockStride = cv::Size(8,8);
	cellSize = cv::Size(8,8);
	numBins = 9;
}

void HOG::ComputeDescriptor(cv::Mat &frame)
{
	cv::Mat I_r;
    cv::resize(frame,I_r,cv::Size(winSize.width+2,winSize.height+2),0,0,cv::INTER_CUBIC);

	cv::HOGDescriptor hog;
    hog.winSize = winSize;
	hog.blockSize = blockSize;
	hog.blockStride = blockStride;
	hog.cellSize = cellSize;
	hog.nbins = numBins;
	hog.compute(I_r, descriptor);
}
HOG::~HOG(void)
{
}
