#include "motionEstimator.h"


motionEstimator::motionEstimator(void)
{
    this->kill_flag = false;
    this->updated_pose = false;
    this->running = false;
}


motionEstimator::~motionEstimator(void)
{
    kill();
}

void motionEstimator::KalmanFilterThread(){
    bool kill_flag;
    // Get kill flag
    kill_flag = getKillFlag();

    // Init Kalman filter
    cv::KalmanFilter KF(6, 3, 0);
    //    KF.transitionMatrix = *(cv::Mat_<double>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
    float delta_t = 1;
    KF.transitionMatrix = *(cv::Mat_<float>(6, 6) << 1,0,0,delta_t,0,0,   0,1,0,0,delta_t,0,  0,0,1,0,0,delta_t,  0,0,0,1,0,0, 0,0,0,0,1,0, 0,0,0,0,0,1);
    cv::Mat_<float> measurement(3,1); measurement.setTo(cv::Scalar(0));

    cv::Mat p = getMeasuredPosition();

    KF.statePre.at<float>(0) = (float) p.at<double>(0);
    KF.statePre.at<float>(1) = (float) p.at<double>(1);
    KF.statePre.at<float>(2) = (float) p.at<double>(2);
    KF.statePre.at<float>(3) = 0;
    KF.statePre.at<float>(4) = 0;
    KF.statePre.at<float>(5) = 0;
    cv::setIdentity(KF.measurementMatrix);
    cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-4));
    cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-1));
    cv::setIdentity(KF.errorCovPost, cv::Scalar::all(.1));

//    cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-1));
//    cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(10));
//    cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1));

    cv::Mat samples(BUFFER_SIZE_KALMAN, 2,CV_64F);
    int meancount = 0;
    double dist = 0;
    boost::posix_time::time_duration dt;

    boost::posix_time::ptime t, t_prev;
    t_prev = boost::posix_time::microsec_clock::local_time();


    while(!kill_flag){

        // Kalman filter update
        // First predict, to update the internal statePre variable

        //Compute processing time
        t = boost::posix_time::microsec_clock::local_time();
        dt = t - t_prev;
        delta_t = (float) dt.total_microseconds();

        // Update transition matrix
        KF.transitionMatrix.at<float>(0, 3) = delta_t / (float) 1E6;
        KF.transitionMatrix.at<float>(1, 4) = delta_t / (float) 1E6;
        KF.transitionMatrix.at<float>(2, 5) = delta_t / (float) 1E6;

        // Update time
        t_prev = t;

        cout << "delta_t: " << delta_t << endl;

        // Predict
        cv::Mat prediction = KF.predict();

        if (isUpdated()){
            // Get new pose mesaurement
            p = getMeasuredPosition();

            // Covariance and Mahalanobis distance calculation
            //Creating samples vectors
            if (meancount < BUFFER_SIZE_KALMAN){
                meancount++;
            }
            if(meancount > 1){
                //Shifting buffer
                for (int i=1; i < meancount; i++){
                    samples.at<double>(i-1, 0) = samples.at<double>(i, 0);  // Column one: predicted value
                    samples.at<double>(i-1, 1) = samples.at<double>(i, 1);  // Column two: measurement
                }
            }
            samples.at<double>(meancount-1, 0) = prediction.at<double>(0);
            samples.at<double>(meancount-1, 1) = p.at<double>(0);

            if (meancount == BUFFER_SIZE_KALMAN){

                //                    cout  << samples << endl;

                // Calculating Covariance Matrix
                cv::Mat Q,invQ, m;
                cv::calcCovarMatrix(samples, Q, m, CV_COVAR_NORMAL+CV_COVAR_ROWS, CV_64F);
                Q /= (samples.rows - 1);

                //                    cout << Q << endl << endl;

                //Inverting covariance Matrix
                cv::invert(Q,invQ,cv::DECOMP_SVD);

                //Computing Mahalanobis distance
                cv::Mat v1(1, 2, CV_64F);   // Current state
                cv::Mat v2(1, 2, CV_64F);   // Previous state

                v1.at<double>(0) = prediction.at<double>(0);
                v1.at<double>(1) = p.at<double>(0);

                v2.at<double>(0) = samples.at<double>(meancount-2, 0);
                v2.at<double>(1) = samples.at<double>(meancount-2, 1);


                dist = cv::Mahalanobis(v1, v2, invQ);

                cout << "Mahalanobis: " << dist << endl;
            }

            // Saturate the measurement
            if (dist < SATURATION_VALUE){

                measurement(0) = (float) p.at<double>(0);
                measurement(1) = (float) p.at<double>(1);
                measurement(2) = (float) p.at<double>(2);

                //        cv::Point measPt(measurement(0),measurement(1));

                // The "correct" phase that is going to use the predicted value and our measurement
                cv::Mat estimated = KF.correct(measurement);
                estimated.copyTo(prediction);
                //        cv::Point statePt(estimated.at<double>(0),estimated.at<double>(1));
            }

        }

        //        // Set position and speeds
        //        cout << "prediciton: " << prediction.rows << "   " << prediction.cols << endl;
        setEstimatedPose(prediction(cv::Rect(0,0,1,3)));
        //        cout << prediction(cv::Rect(0,0,1,2)) << endl;
        setEstimatedSpeed(prediction(cv::Rect(0,3,1,3)));
        setRunning();

        // Get kill flag
        kill_flag = getKillFlag();

        // Sleep to make it periodic
        boost::this_thread::sleep(boost::posix_time::milliseconds(this->period));
    }
}

int motionEstimator::run(double frequency, int method){
    if (method == MOTION_KALMAN_FILTER){
        this->period = (1.d / frequency) * 1000;    // In miliseconds
        // Start thread
        this->thread = new boost::thread(boost::bind(&motionEstimator::KalmanFilterThread, this));
        return SUCCESS;
    }
    else{
        return ERROR;
    }
}

int motionEstimator::setEstimatedPose(cv::Mat p){
    cv::Mat local = cv::Mat(3, 1, CV_64F);
    local.at<double>(0) = (double) p.at<float>(0);
    local.at<double>(1) = (double) p.at<float>(1);
    local.at<double>(2) = (double) p.at<float>(2);
    mtx_.lock();
    estimated_pose = local.clone();
    mtx_.unlock();

    return SUCCESS;
}

int motionEstimator::setEstimatedSpeed(cv::Mat p){
    cv::Mat local = cv::Mat(3, 1, CV_64F);
    local.at<double>(0) = (double) p.at<float>(0);
    local.at<double>(1) = (double) p.at<float>(1);
    local.at<double>(2) = (double) p.at<float>(2);
    mtx_.lock();
    estimated_speed = local.clone();
    mtx_.unlock();

    return SUCCESS;
}

int motionEstimator::kill(){
    mtx_.lock();
    this->kill_flag = true;
    mtx_.unlock();

    return SUCCESS;
}

int motionEstimator::updateMeasuredPosition(cv::Mat p){
    mtx_.lock();
    this->measured_position = p.clone();
    updated_pose = true;
    mtx_.unlock();

    return SUCCESS;
}

bool motionEstimator::isUpdated(){
    bool isUpdated;
    mtx_.lock();
    isUpdated = updated_pose;
    mtx_.unlock();

    return isUpdated;
}

cv::Mat motionEstimator::getMeasuredPosition(){
    cv::Mat local;
    mtx_.lock();
    local = measured_position.clone();
    updated_pose = false;
    mtx_.unlock();

    return local;
}

cv::Mat motionEstimator::getEstimatedPosition(){
    cv::Mat local;
    mtx_.lock();
    local = estimated_pose.clone();
    mtx_.unlock();

    return local;
}

cv::Mat motionEstimator::getEstimatedSpeed(){
    cv::Mat local;
    mtx_.lock();
    local = estimated_speed.clone();
    mtx_.unlock();

    return local;
}

bool motionEstimator::getKillFlag(){
    bool kill_flag;

    // Get kill flag
    mtx_.lock();
    kill_flag = this->kill_flag;
    mtx_.unlock();

    return kill_flag;
}

void motionEstimator::setRunning(){
    mtx_.lock();
    this->running = true;
    mtx_.unlock();
}

bool motionEstimator::isRunning(){
    bool local;
    mtx_.lock();
    local = this->running;
    mtx_.unlock();

    return local;
}







