#include "ObjectDetector.h"
using namespace cv;

#define IMSHOW_MODE 0

ObjectDetector::ObjectDetector(void)
{
    //Logistic Regression + HOG
//    Model_name = "../../../src/Helipad_Detection/resources/Modelo_LR_Helipad_48x48_7";
    Model_name = "../../../../src/Helipad_Detection/resources/Modelo_LR_Helipad_48x48_7";
    roiClassifier.SetModel(1, 1, Model_name);


    //SVMs + HOG
    //std::string Model_name = "../../../src/helipad_detection/resources/Modelo_SVM_Linear_Helipad_48x48_1";
    //roiClassifier.SetModel(2, 1, Model_name);
}


ObjectDetector::~ObjectDetector(void)
{

}

void ObjectDetector::setModelName(string name)
{
    Model_name = name;
    roiClassifier.SetModel(1, 1, Model_name);
}

cv::Rect ObjectDetector::DetectHelipad(cv::Mat &I)
{
    cv::Mat I_helipad, I_candidates, I_ROIs_corners, I_helipad_rotated;
    I.copyTo(I_helipad);
    I.copyTo(I_candidates);
    I.copyTo(I_ROIs_corners);
    I.copyTo(I_helipad_rotated);
    cv::Mat I_roi;
    cv::Mat I_roi_warped;
    cv::Rect Helipad_rect;
    std::vector<std::vector<cv::Point> > rectangles;
    std::vector<cv::Point> helipad_contour;
    shapeImageProcessor.findRectangles(I, rectangles);
    shapeImageProcessor.drawRectanglesMask(I, rectangles);




    std::vector<cv::Rect> bounding_rectangles;
    for( size_t i = 0; i < rectangles.size(); i++ )
    {
        bool noError;
        cv::Rect roi = cv::boundingRect(rectangles[i]);
        bounding_rectangles.push_back(roi);

        //std::vector<cv::Point2f> points_2D_sorted;
        //points_2D_sorted = matchOBjectToImageRansac(rectangles[i]);

        I_roi = I(roi);

        noError = GetFrontHeliCandidate(I_candidates, I_roi_warped, cv::Size(I_roi.cols,I_roi.rows), rectangles[i]);
        int clase = roiClassifier.Classify(I_roi_warped);
        if(clase == 1)
        {
            if(IMSHOW_MODE)
            {
                imshow("HelipadCandidate", I_roi_warped);
                imshow("ROI input to the CLASSIFIER", I_roi);
            }
            Helipad_rect = roi;
            helipad_contour = rectangles[i];

        }
        cout<<"**** CLASE ROI ****"<<endl;
        cout<<clase<<endl;
    }

    cout<<"Helipad contour:"<<endl<<helipad_contour<<endl;
    if(!helipad_contour.empty())
    {
        cv::Point2f corner_points[4];
        std::vector<cv::Point2f> corners;
        RotatedRect rotated_helipad = cv::minAreaRect(cv::Mat(helipad_contour));
        rotated_helipad.points(corner_points);
        int offset = 10;
        for( int j = 0; j < 4; j++)
        {
            //cv::circle(I_helipad_rotated, corner_points[j], 3, cv::Scalar(255, 255, 255), 2);
            cv::line(I, corner_points[j], corner_points[(j+1)%4], cv::Scalar(0, 255, 0), 3, 8);

            if(((corner_points[j].x) > offset && (corner_points[j].x < I_ROIs_corners.cols - offset)) && ((corner_points[j].y > offset) && (corner_points[j].y < I_ROIs_corners.rows - offset)))
            {
                cv::circle(I, corner_points[j], 5, cv::Scalar(255, 0, 0), 2);
                corners.push_back(corner_points[j]);
            }
        }
    }

    rectangle(I_helipad, Helipad_rect.tl(), Helipad_rect.br(), cv::Scalar(0,255,0), 3, 8, 0);

    if(IMSHOW_MODE)
    {
        imshow("HELIPAD DETECTION", I_helipad);
        imshow("HELIPAD ROTATED DETECTION", I_helipad_rotated);
        cv::waitKey(1);
    }


    return Helipad_rect;


}


bool ObjectDetector::GetFrontHeliCandidate(cv::Mat &in, cv::Mat &out, cv::Size size, std::vector<cv::Point > points)
{
    if (points.size() != 4)
        return false;

    // obtain the perspective transform
    cv::Point2f pointsRes[4], pointsIn[4];
    for (int i = 0; i < 4; i++)
        pointsIn[i] = points[i];
    pointsRes[0] = (cv::Point2f(0, 0));
    pointsRes[1] = cv::Point2f(size.width, 0);
    pointsRes[2] = cv::Point2f(size.width, size.height);
    pointsRes[3] = cv::Point2f(0, size.height);
    Mat H = getPerspectiveTransform(pointsIn, pointsRes);
    cv::warpPerspective(in, out, H, size, cv::INTER_NEAREST);

    return true;
}



std::vector<cv::Point2f> ObjectDetector::DetectCornersBasedOnContours(cv::Mat &I, cv::Mat &I_binarized)
{
	//Search for CONTOURS 
	cv::Mat I_contours, I_ROI_boxes, I_ROIs_rotated, I_ROIs_corners, I_thresh;
	I.copyTo(I_contours);
	I.copyTo(I_ROI_boxes);
	I.copyTo(I_ROIs_rotated);
	I.copyTo(I_ROIs_corners);
	I_binarized.copyTo(I_thresh);
    std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Point2f> corners;

	//cv::findContours(I_color_result, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	cv::findContours(I_thresh, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);


    std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
	std::vector<cv::Rect> box_contours(contours.size());
	int size_MaxContour=0;
    //cout<<"**** Area de los Contornos ****"<<endl;
	for(int i=0;i<contours.size();i++)
	{
		box_contours[i] = cv::boundingRect(cv::Mat(contours[i]));
		if(box_contours[i].area()>size_MaxContour)
			size_MaxContour = box_contours[i].area();
        //cout<<"contorno i: "<<box_contours[i].area()<<endl;
	}
    //cout<<"contorno maximo:"<<endl<<size_MaxContour<<endl;
	
	itc = contours.begin();
	while(itc!=contours.end())
	{
        Rect box = cv::boundingRect(cv::Mat(*itc));
        if(box.area()<size_MaxContour/4)
            itc = contours.erase(itc);
		else
			++itc;
	}


	vector<RotatedRect> minRect(contours.size());
	box_contours.clear();
	int offset = 10;
	for(int i=0;i<contours.size();i++)
	{
		minRect[i] = minAreaRect( Mat(contours[i]));
		//box_contours.push_back(cv::boundingRect(cv::Mat(contours[i])));
		//cv::rectangle(I_ROI_boxes, box_contours[i], cv::Scalar(0, 255, 0),2);

		//cv::circle(I_ROI_boxes, box_contours[i].tl(), 3, cv::Scalar(0,128, 255),3);
		//cv::circle(I_ROI_boxes, cv::Point(box_contours[i].tl().x + box_contours[i].width, box_contours[i].tl().y), 3, cv::Scalar(0,128, 255),3);
		//cv::circle(I_ROI_boxes, box_contours[i].br(), 3, cv::Scalar(0,128, 255),3);
		//cv::circle(I_ROI_boxes, cv::Point(box_contours[i].br().x - box_contours[i].width, box_contours[i].br().y), 3, cv::Scalar(0,128, 255),3);

		Point2f rect_points[4]; 
		minRect[i].points(rect_points);
		for( int j = 0; j < 4; j++ )
		{
			//cv::circle(I_ROIs_rotated, rect_points[j], 3, cv::Scalar(255, 255, 255), 2); 
            cv::line(I_ROIs_rotated, rect_points[j], rect_points[(j+1)%4], cv::Scalar(0, 255, 0), 2, 8);

			if(((rect_points[j].x) > offset && (rect_points[j].x < I_ROIs_corners.cols - offset)) && ((rect_points[j].y > offset) && (rect_points[j].y < I_ROIs_corners.rows - offset)))
			{
				cv::circle(I_ROIs_corners, rect_points[j], 3, cv::Scalar(255, 255, 255), 2);
				corners.push_back(rect_points[j]);
			}
		}
	}
	//cv::imshow("I ROI BOXES", I_ROI_boxes);
    cv::imshow("I ROTATED ROIs", I_ROIs_rotated);
	//cv::imshow("I ROTATED CORNERS", I_ROIs_corners);
	//writer.write(I_ROIs_corners);

	return corners;
}



