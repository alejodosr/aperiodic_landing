#include "RoiClassifier.h"
#include <time.h> 
#include <ctime>
using namespace std;
using namespace cv;

RoiClassifier::RoiClassifier(void)
{
}

void RoiClassifier::Init()
{
	classifier = new LogisticRegression;
	//classifier = new SVM_Classifier;
	feature_descriptor = new HOG;
	classifier->LoadModel();
}


void RoiClassifier::SetModel(int classifier_type, int descriptor_type, std::string Model_name)
{
	clock_t start,end;
	double elapsedTime=0.0;
	const char * Model_name_c = Model_name.c_str();

	switch(classifier_type)
	{
		case 1:
			classifier = new LogisticRegression;
			break;
        case 2:
            classifier = new SVM_Classifier;
            break;
//		case 3:
//			classifier = new MLP_Classifier;
//			break;
        default:
            classifier = new LogisticRegression;
            break;
	}
	switch(descriptor_type)
	{
		case 1:
			feature_descriptor = new HOG;
			break;
        default:
            feature_descriptor = new HOG;
            break;
	}
	classifier->SetModel(Model_name);
}

int RoiClassifier::Classify(cv::Mat &I_roi)
{
	window_probs.clear();
	feature_descriptor->ComputeDescriptor(I_roi);
	std::vector<float> desc = feature_descriptor->GetDescriptor();
	classifier->predict(desc);
	window_probs = classifier->GetProbabilities();
    cout<<"ROI probabilities:"<<endl;
    cout<<"["<<window_probs[0]<<" ; "<<window_probs[1]<<"]"<<endl;

	int window_class=0;
    if(window_probs[0]>0.999 && window_probs[1]<0.001) //Classifier #6
    //if(window_probs[0]>0.999 && window_probs[1]<0.0001) //Classifier #7
    //if(window_probs[0]>0.99 && window_probs[1]<0.1) //Classifier #3 BOVW
		window_class = 1;
	else 
		window_class = 0;

	return window_class;
}

RoiClassifier::~RoiClassifier(void)
{
}
