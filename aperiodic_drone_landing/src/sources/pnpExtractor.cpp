#include "pnpExtractor.h"


pnpExtractor::pnpExtractor(void)
{
    //Logistic Regression + HOG
//    Model_name = "../../../src/Helipad_Detection/resources/Modelo_LR_Helipad_48x48_7";
//    Model_name = "../../../../src/Helipad_Detection/resources/Modelo_LR_Helipad_48x48_7";
//    roiClassifier.SetModel(1, 1, Model_name);

    //SVMs + HOG
    //std::string Model_name = "../../../src/helipad_detection/resources/Modelo_SVM_Linear_Helipad_48x48_1";
    //roiClassifier.SetModel(2, 1, Model_name);
}


pnpExtractor::~pnpExtractor(void)
{

}

int pnpExtractor::setCameraParameters(cv::Mat cameraMatrix, cv::Mat distCoeffs){
    this->cameraMatrix = cameraMatrix;
    this->distCoeffs = distCoeffs;
    return SUCCESS;
}

int pnpExtractor::computePnP(std::vector<cv::Point3f> objectPoints, std::vector<cv::Point2f> imagePoints, int method){

    if (method == PNP_RANSAC){
        cv::solvePnPRansac(objectPoints, imagePoints, cameraMatrix, distCoeffs, rvec, tvec);
        return SUCCESS;
    }
    if (method == PNP_ITER){
        cv::solvePnP(objectPoints, imagePoints, cameraMatrix, distCoeffs, rvec, tvec);
        return SUCCESS;
    }
    else{
        return ERROR;
    }
}

cv::Mat pnpExtractor::getRvec(){
    return rvec;
}

cv::Mat pnpExtractor::getTvec(){
    return tvec;
}

std::vector<cv::Point2d>  pnpExtractor::project3Dto2D(std::vector<cv::Point3d> point3D, cv::Mat tvec_in){
    std::vector<cv::Point2d> point2D;
    cv::Mat rvecR;
    cv::Rodrigues(rvec,rvecR);
    cv::projectPoints(point3D, rvec, tvec_in, cameraMatrix, distCoeffs, point2D);
    return point2D;
}

//bool ObjectDetector::GetFrontHeliCandidate(cv::Mat &in, cv::Mat &out, cv::Size size, std::vector<cv::Point > points)
//{
//    if (points.size() != 4)
//        return false;

//    // obtain the perspective transform
//    cv::Point2f pointsRes[4], pointsIn[4];
//    for (int i = 0; i < 4; i++)
//        pointsIn[i] = points[i];
//    pointsRes[0] = (cv::Point2f(0, 0));
//    pointsRes[1] = cv::Point2f(size.width, 0);
//    pointsRes[2] = cv::Point2f(size.width, size.height);
//    pointsRes[3] = cv::Point2f(0, size.height);
//    Mat H = getPerspectiveTransform(pointsIn, pointsRes);
//    cv::warpPerspective(in, out, H, size, cv::INTER_NEAREST);

//    return true;
//}







