/*
 *
 *
 *  Created on: July 12th 2016
 *      Author: Milosevic Zorana
 */

#include "droneAperiodicLandingROSModule.h"




int main(int argc, char **argv)
{

    ros::init(argc, argv, "DroneAperiodicLandingROSModule");
    //ros::AsyncSpinner spinner(1);
    ros::NodeHandle n;

    //Init
    std::cout<<"Starting DroneAperiodicLanding..."<<std::endl;

    //DroneAperiodicLanding
    DroneAperiodicLanding MyDroneAperiodicLandingROSModule;
    MyDroneAperiodicLandingROSModule.open(n,"DroneAperiodicLandingROSModule");
    MyDroneAperiodicLandingROSModule.MyHelipadDetectorROSModule.open(n);

    try
    {
				while(ros::ok()){
					ros::spin();
        //spinner.start();
     //   while(ros::ok())
      //  MyDroneAperiodicLandingROSModule.run();
        //spinner.stop();
				}        
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

    return 1;

}

