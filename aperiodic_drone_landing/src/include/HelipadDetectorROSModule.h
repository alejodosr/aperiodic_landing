
#ifndef _HELIPAD_DETECTOR_ROS_MODULE_H
#define _HELIPAD_DETECTOR_ROS_MODULE_H

#include <iostream>
#include <string>
#include <vector>
#include "ObjectDetector.h"
#include "pnpExtractor.h"
#include "motionEstimator.h"
#include <aruco.h>

// ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseStamped.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include "boost/thread.hpp"
#include "boost/bind.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>

#define COMPLETE_TEST
#define HELIPAD_SIZE 0.27
#define MIN_DISTANCE_TO_LAND 6  // Meters far from the UAV to execute the landing stage
#define GAZEBO_CAM
#define WITH_ARUCO

#define IMSHOW 0


class HelipadDetectorROSModule
{
private:
    ObjectDetector objectDetector;
    pnpExtractor pnp_extractor;
    motionEstimator motion_estimator;
    cv_bridge::CvImagePtr cvBottomImage;
    cv::Mat bottom_camera_image;
    cv::Mat I_trajectory = cv::Mat::zeros(480, 640, CV_8UC3);
    // Debug
    float max_x_actual = 0, max_y_actual = 0, max_z_actual = 0, max_x_est = 0, max_y_est = 0, max_z_est = 0;
    //
    cv::Point actualPoint = cv::Point(0,0);
    cv::Point previousPoint = actualPoint;
    cv::Point actualEstimatedPoint= cv::Point(0,0);
    cv::Point previousEstimatedPoint = actualEstimatedPoint;

#ifdef GAZEBO_CAM
    float camera_parameters[9] = { 457.849998, 0.000000, 320.5, 0.000000, 457.849998, 240.5, 0.000000, 0.000000, 1.000000};
    cv::Mat camera_matrix = cv::Mat(3, 3, CV_32F, camera_parameters);
    float distortion_parameters[9] = { 0.0, 0.0, 0.0, 0.0, 0.0};
    cv::Mat distortion_matrix = cv::Mat(4, 1, CV_32F, distortion_parameters);
#endif

    //ROS Publishers
    ros::Publisher helipad_recognized_image_pub;

    //ROS Subscribers
    ros::Subscriber drone_image_from_bottom_camera_subs;

    // Mutex
    boost::mutex mtx_;

    double estimated_speed;
    bool is_v_estimated;


public:
    HelipadDetectorROSModule();
    ~HelipadDetectorROSModule();


    void readParameters();
    void droneImageFromBottomCameraCallback(const sensor_msgs::Image &msg);
    void open(ros::NodeHandle & nIn);
    int setEstimatedSpeed(double v);
    double getEstimatedSpeed();
    int setEstimatedSpeedFlag(bool v);
    bool getEstimatedSpeedFlag();
    ros::Publisher ObjectRoiPubl;
    ros::Publisher TypeObjectPubl;
    ros::Publisher TwistPubl;



};


#endif
