//#pragma once

#ifndef _ROICLASSIFIER_H_
#define _ROICLASSIFIER_H_


#include "linear.h"
#include "Classifier.h"
#include "FeatureDescriptor.h"
#include "LogisticRegression.h"
#include "SVM_Classifier.h"
#include "HOG.h"
#include <opencv2/opencv.hpp>
#include <iostream>

class RoiClassifier
{
private:
	int classifier_type;
	struct model *model_LogisticReg;
	std::vector<double> window_probs;

	Classifier *classifier;
	FeatureDescriptor *feature_descriptor;

public:
	void Init();
	int Classify(cv::Mat &I_roi);
	void SetModel(int classifier_type,int descriptor_type,std::string Model_name);
    inline std::vector<double> GetWindowProbabilities(){return window_probs;}
	RoiClassifier(void);
	~RoiClassifier(void);
};

#endif

