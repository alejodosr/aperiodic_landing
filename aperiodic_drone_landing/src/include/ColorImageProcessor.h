
#ifndef _COLORIMAGEPROCESSOR_H_
#define _COLORIMAGEPROCESSOR_H_

#include "opencv2/opencv.hpp"
//#include "opencv2/nonfree/features2d.hpp"

class ColorImageProcessor
{
public:
	cv::Mat ProcessInOpponentColorSpace(cv::Mat &I);
	cv::Mat ProcessRedInOpponentColorSpace(cv::Mat &I);
	cv::Mat ProcessBlueInOpponentColorSpace(cv::Mat &I);
	cv::Mat ProcessInRGBColorSpace(cv::Mat &I);
	cv::Mat ProcessInLabColorSpace(cv::Mat &I);
	cv::Mat ProcessInHLSColorSpace(cv::Mat &I);
	ColorImageProcessor(void);
	~ColorImageProcessor(void);
};

#endif

