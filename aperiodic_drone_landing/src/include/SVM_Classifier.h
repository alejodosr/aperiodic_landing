//#pragma once

#ifndef _SVMCLASSIFIER_H_
#define _SVMCLASSIFIER_H_

#include "Classifier.h"
#include "svm.h"
#include <vector>
#include <iostream>

class SVM_Classifier: public Classifier
{
private:
	struct svm_model *model;
	double *prob_estimates;
	svm_node* testnode;
public:
	virtual void LoadModel();
	void SetModel(std::string Model_name);
	void predict(std::vector<float> descriptor);
	SVM_Classifier(void);
	~SVM_Classifier(void);
};

#endif

