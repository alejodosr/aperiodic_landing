//#pragma once

#ifndef _FEATUREDESCRIPTOR_H_
#define _FEATUREDESCRIPTOR_H_

#include <opencv2/opencv.hpp>
class FeatureDescriptor
{
protected:
	std::vector<float> descriptor;
public:
	virtual void ComputeDescriptor(cv::Mat &frame)=0;
    inline std::vector<float> GetDescriptor(){return descriptor;}
	FeatureDescriptor(void);
	~FeatureDescriptor(void);
};

#endif

