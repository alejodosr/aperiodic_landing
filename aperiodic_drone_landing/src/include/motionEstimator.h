
#ifndef _MOTIONESTIMATOR_H_
#define _MOTIONESTIMATOR_H_

#include "opencv2/opencv.hpp"
#include "boost/thread.hpp"
#include "boost/bind.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>
//#include "ColorImageProcessor.h"
//#include "ShapeImageProcessor.h"
//#include "RoiClassifier.h"
//#include "linear.h"

#define MOTION_KALMAN_FILTER    0x0

#define SATURATION_VALUE        0.5
#define BUFFER_SIZE_KALMAN      100

#define SUCCESS                 0x0
#define ERROR                   0x1

using namespace std;

class motionEstimator
{
private:
    bool kill_flag, updated_pose;
    cv::Mat p;
    boost::thread  *thread;
    cv::Mat estimated_pose, estimated_speed, measured_position;
    float period;
    bool running;


    // Mutex
    boost::mutex mtx_;

    // Methods
    void KalmanFilterThread();
    bool getKillFlag();
    bool isUpdated();
    int setEstimatedPose(cv::Mat p);
    int setEstimatedSpeed(cv::Mat p);
    cv::Mat getMeasuredPosition();
    void setRunning();

public:

    // Estimator methods
    int run(double frequency, int method);
    int kill();
    int updateMeasuredPosition(cv::Mat p);
    bool isRunning();


    // Getters
    cv::Mat getEstimatedPosition();
    cv::Mat getEstimatedSpeed();

    motionEstimator(void);
    ~motionEstimator(void);
};

#endif

