
#ifndef _OBJECTDETECTOR_H_
#define _OBJECTDETECTOR_H_

#include "opencv2/opencv.hpp"
#include "ColorImageProcessor.h"
#include "ShapeImageProcessor.h"
#include "RoiClassifier.h"
#include "linear.h"

using namespace std;

class ObjectDetector
{
private:
    struct model *model_LogisticReg;
	ColorImageProcessor colorImageProcessor;
    ShapeImageProcessor shapeImageProcessor;
    RoiClassifier roiClassifier;
    std::string Model_name;
	//std::vector<cv::Point2f> corners_2D;
public:
    cv::Rect DetectHelipad(cv::Mat &I);
	std::vector<cv::Point2f> DetectCornersBasedOnContours(cv::Mat &I, cv::Mat &I_binarized);
    bool GetFrontHeliCandidate(cv::Mat &in, cv::Mat &out, cv::Size size, std::vector<cv::Point > points);
    void setModelName(std::string name);
	//std::vector<cv::Point2f> getObjectCorners(){return corners_2D;}
	ObjectDetector(void);
	~ObjectDetector(void);
};

#endif

