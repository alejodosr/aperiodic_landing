//#pragma once

#ifndef _LOGISTIC_REGRESSION_H_
#define _LOGISTIC_REGRESSION_H_

#include "Classifier.h"
#include "linear.h"
#include <vector>
#include <iostream>

class LogisticRegression: public Classifier
{
private:
	feature_node* testnode;
	struct model *model;
	double *prob_estimates;
public:
	virtual void LoadModel();
	void SetModel(std::string Model_name);
	void predict(std::vector<float> descriptor);
	LogisticRegression(void);
	~LogisticRegression(void);
};

#endif

