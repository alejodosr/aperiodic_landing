//#pragma once

#ifndef _HOG_H_
#define _HOG_H_

#include "FeatureDescriptor.h"
class HOG:public FeatureDescriptor
{
private:
	cv::Size winSize;
	cv::Size blockSize;
	cv::Size blockStride;
	cv::Size cellSize;
	int numBins;
public:
	void ComputeDescriptor(cv::Mat &frame);
	HOG(void);
	~HOG(void);
};

#endif

