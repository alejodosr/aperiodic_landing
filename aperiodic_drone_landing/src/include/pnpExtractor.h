
#ifndef _PNPEXTRACTOR_H_
#define _PNPEXTRACTOR_H_

#include "opencv2/opencv.hpp"
//#include "ColorImageProcessor.h"
//#include "ShapeImageProcessor.h"
//#include "RoiClassifier.h"
//#include "linear.h"

#define PNP_RANSAC 0x0
#define PNP_ITER   0x1

#define SUCCESS    0x0
#define ERROR 0x1

using namespace std;

class pnpExtractor
{
private:

    cv::Mat cameraMatrix, distCoeffs;
    cv::Mat rvec, tvec;

public:

    cv::Mat getRvec();
    cv::Mat getTvec();
    int setCameraParameters(cv::Mat cameraMatrix, cv::Mat distCoeffs);
    int computePnP(std::vector<cv::Point3f> objectPoints, std::vector<cv::Point2f> imagePoints, int method);
    vector<cv::Point2d>  project3Dto2D(vector<cv::Point3d> point3D, cv::Mat tvec_in);



    pnpExtractor(void);
    ~pnpExtractor(void);
};

#endif

