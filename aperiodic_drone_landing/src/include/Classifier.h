//#pragma once

#ifndef _CLASSIFIER_H_
#define _CLASSIFIER_H_

#include <vector>
#include <string>

class Classifier
{
protected:
    std::string Model_name;
	std::vector<double> probs;
public:
	virtual void LoadModel()=0;
	virtual void SetModel(std::string Model_name)=0;
	virtual void predict(std::vector<float> descriptor)=0;
    std::vector<double> GetProbabilities(){return probs;}
	Classifier(void);
	~Classifier(void);
};

#endif

