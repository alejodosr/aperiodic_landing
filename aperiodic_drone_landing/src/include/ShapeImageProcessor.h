
#ifndef _SHAPEIMAGEPROCESSOR_H_
#define _SHAPEIMAGEPROCESSOR_H_

#include "opencv2/opencv.hpp"
//#include "opencv2/nonfree/features2d.hpp"

class ShapeImageProcessor
{
public:
    cv::Mat detectRectangles(cv::Mat &I);
    void findRectangles(const cv::Mat &image, std::vector<std::vector<cv::Point> >& squares);
    cv::Mat drawRectanglesMask(cv::Mat &image, const std::vector<std::vector<cv::Point> >& squares);
    cv::Mat drawRectanglesContour(cv::Mat &image, const std::vector<std::vector<cv::Point> >& squares);
    double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0);
	ShapeImageProcessor(void);
	~ShapeImageProcessor(void);
};

#endif

