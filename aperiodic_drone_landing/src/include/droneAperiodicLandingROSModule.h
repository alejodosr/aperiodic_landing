/*
 *
 *
 *  Created on: 8th February 2016
 *      Author: Alejandro Rodríguez-Ramos
 *
 * */


#ifndef DRONE_LANDING_H
#define DRONE_LANDING_H

#include "ros/ros.h"

#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include "std_msgs/String.h"
#include "droneModuleROS.h"
#include "geometry_msgs/PoseStamped.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/dronePose.h"
#include <sensor_msgs/Range.h>
#include <boost/thread/thread.hpp>
#include "HelipadDetectorROSModule.h"

#define ALTITUDE_THRESHOLD          0.3     // Height of the platform
#define ALTITUDE_TO_LAND            0.45    // Altitude to get ready to land
#define BUFFER_SIZE                 5       // Buffer of the altitude values
#define MAX_COUNTED_PLATFORMS       1
//#define PERCENTAGE_SIZE           0.8     // Percentage (up to 1) of the platform which enables to land
#define SAFETY_DISTANCE             0.7     // Distance below the drone to safely land
#define LANDING_GEAR_WIDTH          0.4
//#define TIME_TO_FALL                0.6     // Time the drone requires to fall on the moving platform

// IMAV configuration
#define TIME_TO_FALL                0.30     // Time the drone requires to fall on the moving platform


#define PLATFORM_SIZE               1.42     // Size in meters
#define PERCENTAGE_TOL              0.2
#define SAFE_ALTITUDE               0.2     // Maximum safe altitude
#define DEBUG_MODE
#define SIM_MODE
#define ALTITUDE_INTERFIERENCE
#ifdef ALTITUDE_INTERFIERENCE
    #define ALTITUDE_STEP           0.1
#endif


class DroneAperiodicLanding: public DroneModule
{	

private:
    ros::Subscriber droneLidar;
    ros::Subscriber droneRebroadCast;
    ros::Subscriber dronePoseSub;

    ros::Publisher dronePosePub;
    ros::Publisher DroneCommandPubl;

private:
#ifdef SIM_MODE
    void droneLidarCallback(const geometry_msgs::PoseStamped &msg);
#else
    void droneLidarCallback(const sensor_msgs::Range &msg);
#endif

    void dronePoseCallback(const droneMsgsROS::dronePose &msg);
//    void publishDronePositionReference( double x, double y, double z);
//    void droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg);
    void DroneLanding();

    bool start_landing;

    float measuredAltitude;
    droneMsgsROS::dronePose current_pose;
    bool enable_start, enable_stop, thread_started;

    boost::thread landing_thread;

    ros::Time start_peak, stop_peak;
    int counted_platforms;

    std::vector<double> altitudes;
    std::vector<double> platform_sizes;
    double platform_size, landing_time;
		double landing_x, landing_y;


public:
    DroneAperiodicLanding();
    ~DroneAperiodicLanding();
	
    void open(ros::NodeHandle & nIn, std::string moduleName);
	void close();

protected:
    bool init();
    void readParameters();

protected:
    bool resetValues();


public:
    bool run();
    // Helipad Detector instance
    HelipadDetectorROSModule MyHelipadDetectorROSModule;



};


#endif
